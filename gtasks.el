;;; gtasks.el --- Google Tasks client -*- lexical-binding: t -*-

;; Author: Kisaragi Hiu
;; Maintainer: Kisaragi Hiu
;; Version: 0.0.1
;; Package-Requires: ((emacs "26.1") (oauth2 "0.16") (oauth2-request "1.0"))
;; Homepage: https://kisaragi-hiu.com/projects/gtasks.el
;; Keywords: convenience


;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; A Google Tasks client for Emacs. Praying that Google will not
;; change its API.

;;; Code:

(require 'oauth2)
(require 'oauth2-request)

;; https://github.com/misohena/gcal/blob/master/gcal.el
(defvar gtasks-client-id "163356184716-i7s3etnkhs0kel7o3c3sbu66rh4cguqe.apps.googleusercontent.com")
(defvar gtasks-client-secret "k4o9gXvuZIokPITb9o75UcgU")

(defvar gtasks-token
  (oauth2-auth
   "https://accounts.google.com/o/oauth2/auth"
   "https://oauth2.googleapis.com/token"
   gtasks-client-id
   gtasks-client-secret
   "https://www.googleapis.com/auth/tasks https://www.googleapis.com/auth/tasks.readonly"))

(defun gtasks-tasklists-get ()
  "Return tasklists."
  (catch 'ret
    (oauth2-request
        gtasks-token
        "https://tasks.googleapis.com/tasks/v1/users/@me/lists"
      :sync t
      :complete (cl-function
                 (lambda (&key response &allow-other-keys)
                   (throw 'ret (json-read-from-string
                                (request-response-data response))))))))

;; DELETE /tasks/v1/users/@me/lists/{tasklist}
;; GET /tasks/v1/users/@me/lists/{tasklist}
;; POST /tasks/v1/users/@me/lists (data: JSON for a TaskList)
;; GET /tasks/v1/users/@me/lists
;; PATCH /tasks/v1/users/@me/lists/{tasklist} (data: JSON for a TaskList)

(gtasks-tasklists-get)

;; We also kind of want to write to/from Org
;; If not, at least display it in a similar way
;;
;; * Task list 1
;; - [ ] Task 1
;; - [ ] Task 2
;; - [ ] Task 3
;; * Task list 2
;; - [ ] Task 1
;; - [ ] Task 2
;; - [ ] Task 3

(provide 'gtasks)

;;; gtasks.el ends here
