;;; gtasks-test.el --- Tests for gtasks.el -*- lexical-binding: t; -*-

(when (require 'undercover nil t)
  (undercover "*.el"))

(require 'ert)
(require 'gtasks)

(ert-deftest tst-remove ()
  ;; Remove last element
  (should (equal (tst-remove "abc[def]" "def")
                 "abc"))
  ;; Remove one of many
  (should (equal (tst-remove "abc[def ghi]" "def")
                 "abc[ghi]"))
  ;; Remove nonexistent
  (should (equal (tst-remove "abc" "def")
                 "abc")))

(provide 'gtasks-tests)
;;; gtasks-test.el ends here
